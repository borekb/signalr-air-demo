# SignalR long polling AIR client demo #

This sample project demonstrates how to communicate between SignalR on the server and an AIR application implemented in ActionScript / Flex when WebSockets are not available (e.g., when the server-side needs to be hosted on Windows Server 2008).


## How to run ##

1. Open `SignalRDemoServer` solution in Visual Studio
2. Open `SignalRDemoClient` in IntelliJ IDEA
3. Run the server
4. Run the client

When you enter a message in a web application, you should see it in the AIR app too.


## Solution overview ##

The technique used utilizes the official JavaScript SignalR client library from Microsoft. It loads this library using an `HTMLLoader` and sets up a bridge between ActionScript and JavaScript. When the server calls a client-side method, it first invokes a JavaScript function which then calls into ActionScript.


### About the server-side ###

There are a couple of things in this single project:

1. **Server-side C# code** that implements SignalR hubs (the ChatHub)
2. **Static JS and jQuery files** that constitute the JavaScript SignalR library (they are downloaded by NuGet rather than being physically part of the project)
3. **index.html** and **embed.html** files that use the JS library and are useful for humans and the AIR app, respectively

As for the last point, **index.html** is basically the [standard ChatHub sample](http://www.asp.net/signalr/overview/signalr-20/getting-started-with-signalr-20/tutorial-getting-started-with-signalr-20) that Microsoft uses to demonstrate SignalR. The **embed.html** file contains all the same linked JavaScript files but no UI - it is meant to be loaded into the Flex application at runtime. Its main purpose is to delegate JS calls to AS3 calls - see its main script block.


### AIR app ###

See the `Main.mxml` file which is thoroughly documented.    